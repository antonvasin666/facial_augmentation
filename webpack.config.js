'use strict';

const path = require('path');
const webpack = require('webpack');
module.exports = {
    mode: 'development',
    entry: {
        main: './src/index.js',
        // Runtime code for hot module replacement
        //hot: 'webpack/hot/dev-server.js',
        // Dev server client for web socket transport, hot and live reload logic
        //client: 'webpack-dev-server/client/index.js?hot=true&live-reload=true',
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
        clean: true,
    },
    //devtool: 'inline-source-map',
    devServer: {
        static: '.',
        //hot: false,
        //client: false,
    },
    plugins: [
        //new webpack.HotModuleReplacementPlugin(),
    ]
};