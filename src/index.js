'use strict';

import * as THREE from 'three';
import { Vector2 } from 'three';
import { Vector3 } from 'three';
import { Float32BufferAttribute } from 'three';
import { BufferAttribute } from 'three';

let renderer = null;
let scene = null;
let camera = null;


function toVectorArray(obj, yBias) {
	const [xArr, yArr, zArr] = obj;

	const points = new Array(xArr.length);
	for (let i = 0; i < xArr.length; ++i) {
		points[i] = new THREE.Vector3(xArr[i], yBias - yArr[i], zArr[i]);
	}

	return points;
}

async function fetchIndices() {
	const [indicesX, indicesY, indicesZ] = await fetch('assets/tri.json').then(response => response.json());
	const indices = new Array(indicesX.length * 3);
	for (let i = 0; i < indicesX.length; ++i) {
		indices[i * 3 + 0] = indicesZ[i];
		indices[i * 3 + 1] = indicesY[i];
		indices[i * 3 + 2] = indicesX[i];
	}
	return indices;
}

async function fetchVideoMasks(height, indices) {
	const videoMeta = await fetch('assets/video_meta.json').then(response => response.json());
	const masks = videoMeta
		.map(meta => {
			const geometry = new THREE.BufferGeometry().setFromPoints(toVectorArray(meta[1], height));
			geometry.setIndex(indices);
			geometry.computeVertexNormals();
			return { geometry };
		});
	return masks;
}

async function fetchStaticPoses(indices) {
	const staticPoses = [...Array(7).keys()]
		.map(id => 'assets/init_teen_transform_poses1_' + id)
		.map(name => {
			return {
				texture: new THREE.TextureLoader().load(`${name}.jpg`),
				pointsRequest: fetch(`${name}_3d.jpg.json`).then(response => response.json())
			};
		});

	const result = (await Promise.all(staticPoses.map(obj => obj.pointsRequest))).map((obj, i) => {
		const points = toVectorArray(obj[0], 260);
		const vertices = points.length;
		const tc = new Float32Array(vertices * 2);
		for (let j = 0; j < vertices; ++j) {
			tc[j * 2 + 0] = points[j].x / 258;
			tc[j * 2 + 1] = points[j].y / 260;
		}

		const geometry = new THREE.BufferGeometry().setFromPoints(points);
		geometry.setAttribute("uv", new Float32BufferAttribute(tc, 2));
		geometry.setIndex(indices);
		geometry.computeVertexNormals();
		geometry.computeTangents();

		const normal = new THREE.Vector3();
		const tangent = new THREE.Vector3();
		const binormal = new THREE.Vector3();
		for (let j = 0; j < vertices; ++j) {
			normal.x += geometry.attributes.normal.array[j * 3 + 0];
			normal.y += geometry.attributes.normal.array[j * 3 + 1];
			normal.z += geometry.attributes.normal.array[j * 3 + 2];

			tangent.x += geometry.attributes.tangent.array[j * 3 + 0];
			tangent.y += geometry.attributes.tangent.array[j * 3 + 1];
			tangent.z += geometry.attributes.tangent.array[j * 3 + 2];
		}

		normal.normalize();
		tangent.normalize();
		binormal.crossVectors(normal, tangent).normalize();

		let heading = new Vector2(-normal.x, -normal.z);
		heading.normalize();

		return {
			geometry,
			texture: staticPoses[i].texture,
			normal, tangent, binormal,
			yaw: Math.acos(normal.z),
			yaw2: Math.asin(heading.x)
		};
	});

	return result;
}

async function fetchFacialMasks(width, height) {
	const indices = await fetchIndices();
	const videoMasks = await fetchVideoMasks(height, indices);
	const staticPoses = await fetchStaticPoses(indices);

	for (let mask of videoMasks) {
		mask.geometry.attributes.uv = staticPoses[0].geometry.attributes.uv;
		mask.geometry.computeTangents();
	}

	const vertexCode = await fetch('vertex.glsl').then(response => response.text());
	const fragmentCode = await fetch('fragment.glsl').then(response => response.text());
	const shaderMaterial = new THREE.ShaderMaterial({
		uniforms: {
			uTex0: { value: staticPoses[0].texture },
			uTex1: { value: staticPoses[1].texture },
			uTex2: { value: staticPoses[2].texture },
			uTex3: { value: staticPoses[3].texture },
			uTex4: { value: staticPoses[4].texture },
			uTex5: { value: staticPoses[5].texture },
			uTex6: { value: staticPoses[6].texture },

			uWeight0: { value: 0 },
			uWeight1: { value: 0 },
			uWeight2: { value: 0 },
			uWeight3: { value: 0 },
			uWeight4: { value: 0 },
			uWeight5: { value: 0 },
			uWeight6: { value: 1 },

			uOpacity: { value: 0.7 },
		},
		vertexShader: vertexCode,
		fragmentShader: fragmentCode,
		//transparent: true,
	});

	const geometry = staticPoses[0].geometry.clone();
	// const material = new THREE.MeshBasicMaterial({ map: staticPoses[0].texture, opacity: 0.6, transparent: true });
	// const mesh = new THREE.Mesh(geometry, material);
	const mesh = new THREE.Mesh(geometry, shaderMaterial);


	return { videoMasks, staticPoses, mesh };
}

async function makeVideoBackground() {
	const video = document.getElementById("video");
	await video.play();
	video.pause();

	const width = video.videoWidth;
	const height = video.videoHeight;

	const videoTexture = new THREE.VideoTexture(video);
	const material = new THREE.MeshBasicMaterial({ map: videoTexture });
	const quad = new THREE.Mesh(new THREE.PlaneGeometry(width, height), material);
	quad.position.x = width / 2;
	quad.position.y = height / 2;
	return { video, quad, width, height };
}

function getYaw(geometry) {
	const vertices = geometry.attributes.normal.count;
	const array = geometry.attributes.normal.array;

	let normal = new THREE.Vector3();
	for (let i = 0; i < vertices; ++i) {
		normal.x += array[i * 3 + 0];
		normal.y += array[i * 3 + 1];
		normal.z += array[i * 3 + 2];
	}
	normal.normalize();
	let yaw = Math.acos(normal.z);

	let heading = new THREE.Vector2(-normal.x, -normal.z);
	heading.normalize();
	let yaw2 = Math.asin(heading.x);

	return yaw2;
}

async function init() {
	renderer = new THREE.WebGLRenderer();
	scene = new THREE.Scene();

	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);

	const videoBackground = await makeVideoBackground();
	scene.add(videoBackground.quad);

	const masks = await fetchFacialMasks(videoBackground.width, videoBackground.height);
	scene.add(masks.mesh);

	camera = new THREE.OrthographicCamera(0, videoBackground.width, videoBackground.height, 0, -1000, 1000);

	const t1 = new Date();
	function animate() {
		const t2 = new Date();
		const elapsedTime = (t2.getTime() - t1.getTime()) / 500.0;
		const step = Math.floor(elapsedTime);
		const t = elapsedTime - step;

		//console.log(elapsedTime, step, t);

		if (step >= masks.videoMasks.length - 1) {
			return;
		}

		const lerp = (t, arrSrc1, arrSrc2, arrDst) => {
			for (let i = 0; i < arrDst.length; ++i) {
				arrDst[i] = arrSrc1[i] * (1.0 - t) + arrSrc2[i] * t;
			}
		};
		lerp(t, masks.videoMasks[step].geometry.attributes.position.array, masks.videoMasks[step + 1].geometry.attributes.position.array, masks.mesh.geometry.attributes.position.array);
		masks.mesh.geometry.attributes.position.needsUpdate = true;
		masks.mesh.geometry.computeVertexNormals();

		for (let i = 0; i < 7; ++i) {
			masks.mesh.material.uniforms[`uWeight${i}`].value = 0;
		}
		const yaw = getYaw(masks.mesh.geometry);

		let leftIdx = -1;
		let rightIdx = 0;
		for (; rightIdx < masks.staticPoses.length; ++leftIdx, ++rightIdx) {
			if (yaw < masks.staticPoses[rightIdx].yaw2) {
				if (rightIdx == 0) {
					leftIdx = 0
				}
				break;
			}
		}
		if (rightIdx == masks.staticPoses.length)
			rightIdx = leftIdx;
		if (leftIdx != rightIdx) {
			const weight = (yaw - masks.staticPoses[leftIdx].yaw2) / (masks.staticPoses[rightIdx].yaw2 - masks.staticPoses[leftIdx].yaw2);
			masks.mesh.material.uniforms[`uWeight${leftIdx}`].value = 1 - weight;
			masks.mesh.material.uniforms[`uWeight${rightIdx}`].value = weight;
		}
		else {
			masks.mesh.material.uniforms[`uWeight${leftIdx}`].value = 1;
		}


		renderer.render(scene, camera);
		requestAnimationFrame(animate);
	}
	requestAnimationFrame(animate);
	videoBackground.video.play();
}


document.getElementById("startButton").onclick = async (ev) => {
	ev.target.parentElement.remove();
	init();
}