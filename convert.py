import sys
import json
import pickle

def convert(input, output):
    in_file = open(input, 'rb')
    in_obj = pickle.load(in_file);

    with open(output, 'wt') as js_file:
        if (isinstance(in_obj, list)):
            json.dump(list(map(lambda x: x.tolist(), in_obj)), js_file)
        else:
            json.dump(in_obj.tolist(), js_file)

convert(*sys.argv[1:])