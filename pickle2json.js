var fs = require('fs');
var spawn = require('child_process').spawn;

const inDir = process.argv[2];
const outDir = process.argv[3];

function loads(pickle, callback) {
    const convert = spawn('python', ['/convert.py', '--loads']);
};

fs.readdir(inDir, (err, files) => {
    files = files.filter((name) => {
        let extenson = name.split('.').pop();
        return extenson == 'pickle' || extenson == 'pkl';
    });

    for (let name of files) {
        const from = `${inDir}/${name}`;
        const newName = name.split('.');
        newName[newName.length - 1] = 'json';
        const to = `${inDir}/${newName.join('.')}`;

        //console.log(from, to);
        spawn('python', ['convert.py', from, to]);
    }
});