#ifdef GL_ES
precision highp float;
#endif

uniform float uOpacity;

uniform sampler2D uTex0;
uniform sampler2D uTex1;
uniform sampler2D uTex2;
uniform sampler2D uTex3;
uniform sampler2D uTex4;
uniform sampler2D uTex5;
uniform sampler2D uTex6;

uniform float uWeight0;
uniform float uWeight1;
uniform float uWeight2;
uniform float uWeight3;
uniform float uWeight4;
uniform float uWeight5;
uniform float uWeight6;

varying vec2 vUV;

void main(void)
{
    gl_FragColor = vec4(
        uWeight0 * texture2D(uTex0, vUV).rgb +
        uWeight1 * texture2D(uTex1, vUV).rgb + 
        uWeight2 * texture2D(uTex2, vUV).rgb + 
        uWeight3 * texture2D(uTex3, vUV).rgb + 
        uWeight4 * texture2D(uTex4, vUV).rgb + 
        uWeight5 * texture2D(uTex5, vUV).rgb + 
        uWeight6 * texture2D(uTex6, vUV).rgb,
        uOpacity);
}